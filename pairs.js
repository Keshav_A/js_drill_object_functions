export function pairs(object) {
    if (typeof object === "object" && !Array.isArray(object) && object !== null) {
        const pairs = [];
        for (let key in object) {
            pairs.push([key, object[key]]);
        }
        return pairs;
    } else {
        console.log("Object required with key and value pairs");
    }
}
