export function keys(object) {
    if (typeof object === "object" && !Array.isArray(object) && object !== null) {
        const keys = [];
        for (let key in object) {
        keys.push(key);
        }
        return keys;
    } else {
        console.log("Object required with key and value pairs");
    }
}
