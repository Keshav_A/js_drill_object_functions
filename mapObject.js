export function increase_value(value) {
    return value + 5;
}

export function map_callback(value) {
    return value;
}

export function mapObject(object, callback) {
    if (typeof object === "object" && !Array.isArray(object) && object !== null) {
        if (typeof callback === "undefined") {
        console.log("Provide a callback function");
        return;
        }
        const new_object = {};
        for (let key in object) {
        new_object[key] = callback(object[key]);
        }
        return new_object;
    } else {
        console.log("Object required with key and value pairs");
    }
}
