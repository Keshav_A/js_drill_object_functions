import { testObject } from "../data.js";
import { reverse } from "../reverse.js";


// Test 1
let key_array = reverse()
// Returns "Object required with key and value pairs"
console.log(key_array)
// Returns "undefined"

//Test 2

key_array = reverse([])
// Returns "Object required with key and value pairs"
console.log(key_array)
// Returns "undefined"

//Test 3

key_array = reverse(1)
// Returns "Object required with key and value pairs"
console.log(key_array)
// Returns "undefined"

//Test 4

key_array = reverse({})
console.log(key_array)
// Returns '{}'

// Test 5

key_array = reverse(testObject)
console.log(key_array)
// Returns
// {36: 'age', Bruce Wayne: 'name', Gotham: 'location'}