import { testObject } from "../data.js";
import { values } from "../values.js";

// Test 1
let key_array = values()
// Returns "Object required with key and value pairs"
console.log(key_array)
// Returns "undefined"

//Test 2

key_array = values([])
// Returns "Object required with key and value pairs"
console.log(key_array)
// Returns "undefined"

//Test 3

key_array = values(1)
// Returns "Object required with key and value pairs"
console.log(key_array)
// Returns "undefined"

//Test 4

key_array = values({})
console.log(key_array)
// Returns '[]'

key_array = values(testObject)
console.log(key_array)
// Returns
// ['Bruce Wayne', 36, 'Gotham']

