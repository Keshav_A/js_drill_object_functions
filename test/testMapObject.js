import { testObject } from "../data.js";
import { increase_value, mapObject } from "../mapObject.js";
import { map_callback } from "../mapObject.js";

// Test 1
let key_array = mapObject();
// Returns "Object required with key and value pairs"
console.log(key_array);
// Returns "undefined"

//Test 2

key_array = mapObject([]);
// Returns "Object required with key and value pairs"
console.log(key_array);
// Returns "undefined"

//Test 3

key_array = mapObject(1);
// Returns "Object required with key and value pairs"
console.log(key_array);
// Returns "undefined"

//Test 4

key_array = mapObject({});
// Returns "Provide a callback function"
console.log(key_array);
// Returns 'undefined'

//Test 5

key_array = mapObject({}, map_callback);
console.log(key_array);
// Returns '{}'

// Test 6

key_array = mapObject(testObject, map_callback);
console.log(key_array);
// Returns
// {name: 'Bruce Wayne', age: 36, location: 'Gotham'}

// Test 7
key_array = mapObject({ a: 1, b: 2, c: 3 }, increase_value);
console.log(key_array);
// Returns
// {a: 6, b: 7, c: 8}
