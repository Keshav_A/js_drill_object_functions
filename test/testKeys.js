import { testObject } from "../data.js";
import { keys } from "../keys.js";

// Test 1
let key_array = keys()
// Returns "Object required with key and value pairs"
console.log(key_array)
// Returns "undefined"

//Test 2

key_array = keys([])
// Returns "Object required with key and value pairs"
console.log(key_array)
// Returns "undefined"

//Test 3

key_array = keys(1)
// Returns "Object required with key and value pairs"
console.log(key_array)
// Returns "undefined"

//Test 4

key_array = keys({})
console.log(key_array)
// Returns '[]'

key_array = keys(testObject)
console.log(key_array)
// Returns
// ['name', 'age', 'location']

