import { testObject } from "../data.js";
import { defaults } from "../default.js";

// Test 1
let key_array = defaults();
// Returns "Object required with key and value pairs"
console.log(key_array);
// Returns "undefined"

//Test 2

key_array = defaults([]);
// Returns "Object required with key and value pairs"
console.log(key_array);
// Returns "undefined"

//Test 3

key_array = defaults(1);
// Returns "Object required with key and value pairs"
console.log(key_array);
// Returns "undefined"

//Test 4

key_array = defaults({});
// Returns "Default Object required as secondary argument"
console.log(key_array);
// Returns "undefined"

// Test 5

key_array = defaults(testObject);
// Returns "Default Object required as secondary argument"
console.log(key_array);
// Returns "undefined"

// Test 6

key_array = defaults(testObject, []);
// Returns "Default Object required as secondary argument"
console.log(key_array);
// Returns "undefined"

// Test 7

key_array = defaults(testObject, 1);
// Returns "Default Object required as secondary argument"
console.log(key_array);
// Returns "undefined"

// Test 8

key_array = defaults(testObject, {});
// Returns
console.log(key_array);
// Returns
// {name: 'Bruce Wayne', age: 36, location: 'Gotham'}
// Test 9

key_array = defaults(testObject, testObject);
// Returns
console.log(key_array);
// Returns
// {name: 'Bruce Wayne', age: 36, location: 'Gotham'}

// Test 10

key_array = defaults(testObject, { height: "198 cm", weight: "204lbs" });
// Returns
console.log(key_array);
// Returns