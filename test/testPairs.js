import { testObject } from "../data.js";
import { pairs } from "../pairs.js";


// Test 1
let key_array = pairs()
// Returns "Object required with key and value pairs"
console.log(key_array)
// Returns "undefined"

//Test 2

key_array = pairs([])
// Returns "Object required with key and value pairs"
console.log(key_array)
// Returns "undefined"

//Test 3

key_array = pairs(1)
// Returns "Object required with key and value pairs"
console.log(key_array)
// Returns "undefined"

//Test 4

key_array = pairs({})
console.log(key_array)
// Returns '[]'

// Test 5
key_array = pairs(testObject)
console.log(key_array)
// Returns
// [Array(2), Array(2), Array(2)]

console.log(JSON.stringify(key_array))
// Returns
// [["name","Bruce Wayne"],["age",36],["location","Gotham"]]


