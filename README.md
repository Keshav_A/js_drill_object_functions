# JS Drill Objects
Build functions to work with objects

## Restrictions
- Recreate the functions from scratch.
- **DONT** Use for example .keys to obtain keys of object, or  .values() to obtain values etc.
- You CAN use concat, push, pop, etc. but do not use the exact method that you are replicating

## File Naming 
Name your function files like so:

`keys.js`

`values.js`

`pairs.js`

And test files 

`testKeys.js`

`testValues.js`

`testPairs.js`

## Problems
1. Do **NOT** use `.keys()` to complete this function. 
    ```
    function keys(obj) {
        // Retrieve all the names of the object's properties.
        // Return the keys as strings in an array.
    }
    ```
2. Do **NOT** use `.values()`, to complete this function.
    ```
    function values(obj) {
        // Return all of the values of the object's own properties.
        // Ignore functions
    }
    ```
3. Function to map over Objects
    ```
    function mapObject(obj, cb) {
        // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    }
    ```
4. Do **NOT** use `.entries()`, to complete this function.
    ```
    function pairs(obj) {
        // Convert an object into a list of [key, value] pairs.
    }
    ```
5. Reverse key and value in Object.
    ```
    function invert(obj) {
        // Returns a copy of the object where the keys have become the values and the values the keys.
        // Assume that all of the object's values will be unique and string serializable.
    }
    ```
6. Fill undefined properties.

    ```
    function defaults(obj, defaultProps) {
        // Fill in undefined properties that match properties on the `defaultProps` parameter object.
        // Return `obj`.
    }
    ```


