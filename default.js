export function defaults(object, defaultObject) {
    if (typeof object === "object" && !Array.isArray(object) && object !== null) {
        if (typeof defaultObject === "object" && !Array.isArray(defaultObject) && defaultObject !== null
        ) {
            console.log(defaultObject);
            for (let key in defaultObject) {
                if (!(key in object)) {
                    object[key] = defaultObject[key];
                }
            }
            return object;
        } else {
            console.log("Default Object required as secondary argument");
        }
    } else {
        console.log("Object required with key and value pairs");
    }
}
