export function reverse(object) {
  if (typeof object === "object" && !Array.isArray(object) && object !== null) {
    const new_object = {};
    for (let key in object) {
      new_object[object[key]] = key;
    }
    return new_object;
  } else {
    console.log("Object required with key and value pairs");
  }
}
