export function values(object) {
    if (typeof object === "object" && !Array.isArray(object) && object !== null) {
        const values = [];
        for (let key in object) {
        values.push(object[key]);
        }
        return values;
    } else {
        console.log("Object required with key and value pairs");
    }
}
